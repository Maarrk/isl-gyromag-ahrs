from base_block import BaseBlock
import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plot



class GiroskopBlok(BaseBlock):
    def __init__(self):
        super(GiroskopBlok, self).__init__()
        self.fi = 0.0
        self.psi = 0.0
        self.teta = 0.0
        self.q0 = np.array([0.0, 0.0, 0.0, 0.0])
        self.dt = 0.5

    def _updated_result(self, step_number):
        v_fi, v_psi, v_teta = self.inputs[0].get_result(step_number)
        self.fi += v_fi * self.dt
        self.psi += v_psi * self.dt
        self.teta += v_teta * self.dt
        w_katy = np.array([v_fi,v_psi,v_teta])
        dqdt = ([dq0dt, dq1dt, dq2dt, q3t])
        qmain = ([-q1, -q2, -q3]
                 [q0, -q3, q2]
                 [q3, q0, -q1]
                 [-q2, q1, q0])
        dqdt =  gmain* w_katy.transpose()
        return self.fi, self.psi, self.teta
