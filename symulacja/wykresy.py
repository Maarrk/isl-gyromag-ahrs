import numpy as np
import matplotlib.pyplot as plt


def natezenia_pola(t, m):
    modul = np.zeros([m.shape[0], 1])
    for i in range(m.shape[0]):
        modul[i] = np.sqrt(m[i, 0] * m[i, 0] + m[i, 1] * m[i, 1] + m[i, 2] * m[i, 2])

    plt.plot(t, m, t, modul)
    plt.xlabel('Czas [s]')
    plt.ylabel('Natezenia pola [G]')
    plt.legend(['X', 'Y', 'Z', 'Modul'])
    plt.grid()
    plt.show()


def katy_orientacji(t, kat):
    kat = np.rad2deg(kat)

    plt.plot(t, kat)
    plt.xlabel('Czas [s]')
    plt.ylabel('Kat [deg]')
    plt.legend(['fi', 'psi', 'teta'])
    plt.grid()
    plt.show()


def katy_orientacji_att(t, att):
    plt.plot(t, att)
    plt.xlabel('Czas [s]')
    plt.ylabel('Kat [deg]')
    plt.legend(['roll', 'pitch', 'yaw'])
    plt.grid()
    plt.show()
