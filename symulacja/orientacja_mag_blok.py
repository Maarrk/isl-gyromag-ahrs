from base_block import BaseBlock
import numpy as np
import qmath as qm


class OrientacjaMagBlok(BaseBlock):
    def __init__(self):
        super(OrientacjaMagBlok, self).__init__()

    def _updated_result(self, step_number):
        # q_obecny = np.array([0, 0, 0, 1])  # stan jednostkowy
        q_obecny = np.array([0, 0, 0.8368120941783026, 0.5474901999460114])  # początkowa orientacja
        b_0 = np.array([-1.15e+02, -1.33e+02, 4.21e+02])  # pierwszy pomiar
        b_zmierzony_b = self.inputs[0].get_result(step_number)[0]
        b_zmierzony_h = qm.rotate(b_zmierzony_b, q_obecny)

        os_obrotu = qm.normalized(np.cross(b_zmierzony_h, b_0))
        cos_a = np.dot(qm.normalized(b_zmierzony_h), qm.normalized(b_0))
        cos_a2 = np.sqrt((1 + cos_a) / 2)
        sin_a2 = np.sqrt(1 - cos_a2 * cos_a2)

        q_delta = np.append(os_obrotu * sin_a2, cos_a2)
        q_delta = np.reshape(q_delta, (4,))
        # print(q_delta)
        # print(qm.quat2eul(q_delta))
        # print(qm.rotate(np.array([1, 0, 0]), q_delta))
        return qm.quat_mul(q_delta, q_obecny)


if __name__ == '__main__':
    o = OrientacjaMagBlok()
    o.get_result(0)
