# isl-gyromag-ahrs

Projekt integracji danych z giroskopów i magnetometru w celu wyznaczenia kątów orientacji przestrzennej.

Autorzy:
- Karol Bresler
- Kamil Dąbrowski
- [Marek Łukasiewicz](http://lukasiewicz.tech/)
- Dariusz Miedziński

Prowadzący: [mgr inż. Sebastian Topczewski](https://www.meil.pw.edu.pl/zaiol/ZAiOL/Pracownicy2/Sebastian-Topczewski)

Projekt wykonany na przedmiot Integracja Systemów Lotniczych, realizowany na Wydziale Mechanicznym Energetyki i Lotnictwa Politechniki Warszawskiej w semestrze letnim 2018/2019.

# Wykorzystane narzędzia

- Kontrola wersji: [Git](https://git-scm.com/)
- Dokumentacja: [LaTeX](https://www.latex-project.org/)
- Obliczenia:
    - Język interpretowany [Python 3.7](https://www.python.org/)
    - Biblioteki obliczeniowe [numpy](http://www.numpy.org/), [scipy](https://www.scipy.org/)
    - Wizualizacja [matplotlib](https://matplotlib.org/)
- Narzędzia:
    - Program do tworzenia diagramów  i schematów blokowych [draw.io](https://www.draw.io/)
    - Pozostałe ilustracje w [Inkscape](https://inkscape.org/)
    - Edytor LaTeX [TeXstudio](https://www.texstudio.org/)
    - Edytor Python [PyCharm](https://www.jetbrains.com/pycharm/)

# Przydatne linki

- Składnia tego dokumentu: [poradnik od GitLab](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/)
- Okienkowy klient do gita [Github Desktop](https://desktop.github.com/)
    - [jak sklonować repozytorium](https://help.github.com/en/desktop/contributing-to-projects/cloning-a-repository-from-github-desktop)
    w kroku 2 wybrać zakładkę URL i wkleić link HTTPS tego repo (niebieski przycisk Clone w prawym górnym rogu tej strony)
- Pliki których nie chcemy synchronizować dodajemy do `.gitignore` [w ten sposób](https://git-scm.com/docs/gitignore)
- [Post na tex.stackoverflow](https://tex.stackexchange.com/questions/41808/how-do-i-install-tex-latex-on-windows) o tym jak można zacząć pracę z LaTeX na Windowsie
- IDE do Texa którego używano: [TeXstudio](https://www.texstudio.org/)
- Stronka do pobierania danych .bib [Citation Machine](http://www.citationmachine.net/)
- Tutorial z którego zacząłem (Marek) [NotSoShort Introduction to LaTeX](https://tobi.oetiker.ch/lshort/lshort.pdf)
- Interaktywny videotutorial [kwaterniony od 3blue1brown](https://eater.net/quaternions)
- Ardupilot to najstabilniejszy autopilot UAV bo ma najlepszy [Extended Kalman Filter](http://ardupilot.org/dev/docs/ekf.html)

# Źródła artykułów:

- [Institute of Electrical and Electronics Engineers](https://www.ieee.org/)
- [American Institute of Aeronautics and Astronautics](https://www.aiaa.org/)
- [Annual of Navigation](http://www.annualofnavigation.pl/)

# Podsumowanie

Dziękujemy za semestr współpracy w ramach tego przedmiotu.

## Uwagi przy prezentacji projektu:

- Można wykorzystać [World Magnetic Model](https://www.ngdc.noaa.gov/geomag/WMM/DoDWMM.shtml)
- W samej prezentacji multimedialnej powinien być jeden wykres na slajdzie żeby był czytelny
- Brakuje numerów slajdów
