%Dariusz Miedziński

\subsection{Kalibracja magnetometru - Dariusz Miedziński}

Pomiary dokonywane za pomocą magnetometru obarczone są błędami wynikającymi z otaczającego czujnik środowiska (elementy żelazne, linie wysokiego napięcia, anteny), jak również wywoływanymi przez sam czujnik. Ze względu na słabe natężenie ziemskiego pola magnetycznego, bezpośrednie wskazania czujnika są bardzo niedokładne.

Zaburzenia pola magnetycznego spowodowane przez pojazd wyposażony w czujnik są stałe, więc mogą być określone ilościowo oraz usunięte z następnych odczytów poprzez algorytm kompensujący przedstawiony w \cite{mag} oraz opisany poniżej.

\subsubsection{Algorytm kompensujący}

 Model magnetometru przyjmuje się w postaci:
\begin{equation}\label{eq1mag}
\tilde{m}=
\begin{bmatrix}
\varepsilon_{1}	&	0	&	0	\\
\varepsilon_{2}sin\rho_{1}	&	\varepsilon_{2}cos\rho_{1}	&	0	\\
\varepsilon_{3}sin\rho_{2}cos\rho_{3}	&	\varepsilon_{3}sin\rho_{3}	&	\varepsilon_{3}cos\rho_{2}cos\rho_{3}	\\
\end{bmatrix}
m+
\begin{bmatrix}
\zeta_{1}	\\	\zeta_{2}	\\	\zeta_{3}\\
\end{bmatrix}
=K_{m}m+b_{m}
\end{equation}
gdzie $\varepsilon_{k}$ jest błędem skalowania czujnika, $\rho_{k}$ jest kątem niewspółosiowości ,$\zeta_{k}$ jest przesunięciem zera czujnika, $\tilde{m}$ jest wektorem natężenia pola magnetycznego otrzymanym z pomiarów, a $m$ jest rzeczywistym (szukanym) wektorem natężenia pola magnetycznego ($k=1,2,3$). Poza błędami czujnika równanie to modeluje również błędy wynikające z dewiacji półokrężnej oraz ćwierćokrężnej.
\newline
Równanie \ref{eq1mag} przekształcamy do postaci $m=K_{m}^{-1}(\tilde{m}-b_{m})$ oraz podstawiamy do równania $m_{1}^{2}+m_{2}^{2}+m_{3}^{2}=|m|^{2}$, gdzie $|m|$ jest stałą\footnote{Jest to wartość wektora natężenia pola magnetycznego możliwa do obliczenia pod adresem http://www.ngdc.noaa.gov/geomagmodels/IGRFWMM.jsp; np. $|m|=0,579 [G]$ w miejscu o współrzędnych $53^\circ25'12''N, 113^\circ23'58''W$.}, otrzymując:
\begin{multline}\label{eq2mag}
C_{1}\tilde{m}_{1}^{2}+C_{2}\tilde{m}_{1}\tilde{m}_{2}+C_{3}\tilde{m}_{1}\tilde{m}_{3}+C_{4}\tilde{m}_{2}^{2}+C_{5}\tilde{m}_{2}\tilde{m}_{3}\\+C_{6}\tilde{m}_{3}^{2}+C_{7}\tilde{m}_{1}+C_{8}\tilde{m}_{2}+C_{9}\tilde{m}_{3}=C_{10}
\end{multline}
gdzie współczynniki $C_{i}$ są funkcjami $\varepsilon,\rho,\zeta$ oraz $|m|$ ($i=1,...,10$). Zakładając, że dysponujemy zbiorem danych z czujnika $\{\tilde{m}\}$ składającym się z $N$ pomiarów, możemy zapisać równanie \ref{eq2mag} jako:
\begin{equation}\label{eq3mag}
\begin{bmatrix}
\tilde{m}_{1,1}^{2}	&	\tilde{m}_{1,1}\tilde{m}_{2,1}	&	\dots	&	\tilde{m}_{3,1}	\\
\vdots	&	\vdots	&	\ddots	& \vdots	\\
\tilde{m}_{1,N}^{2}	&	\tilde{m}_{1,N}\tilde{m}_{2,N}	&	\dots	&	\tilde{m}_{3,N}	\\
\end{bmatrix}
\begin{bmatrix}
C_{1}/C_{10} \\ \vdots \\ C_{9}/C_{10} \\
\end{bmatrix}
=
\begin{bmatrix}
1 \\ \vdots \\ 1 \\
\end{bmatrix}
\end{equation}
z którego następnie metodą najmniejszych kwadratów obliczamy współczynniki $C_{1}/C_{10}\dots C_{9}/C_{10}$. Dostajemy układ dziewięciu równań nieliniowych z niewiadomymi $\varepsilon,\rho,\zeta$, który może zostać rozwiązany numerycznie. Używając $\varepsilon_{k}=1,\rho_{k}=0,\zeta_{k}=0$ jako warunków początkowych rozwiązanie zbiega się stosunkowo szybko. Otrzymane zmienne wstawiamy z powrotem do równania \ref{eq1}, a następnie szukany wektor natężenia pola magnetycznego obliczamy z równania $m=K_{m}^{-1}(\tilde{m}-b_{m})$.

\subsubsection{Wyprowadzenie równań do obliczeń numerycznych}

Wychodząc z rówania \ref{eq1mag}
\begin{equation*}
\tilde{m}=
\begin{bmatrix}
\varepsilon_{1}	&	0	&	0	\\
\varepsilon_{2}sin\rho_{1}	&	\varepsilon_{2}cos\rho_{1}	&	0	\\
\varepsilon_{3}sin\rho_{2}cos\rho_{3}	&	\varepsilon_{3}sin\rho_{3}	&	\varepsilon_{3}cos\rho_{2}cos\rho_{3}	\\
\end{bmatrix}
m+
\begin{bmatrix}
\zeta_{1}	\\	\zeta_{2}	\\	\zeta_{3}\\
\end{bmatrix}
=K_{m}m+b_{m}
\end{equation*}
przekształconego do postaci $m=K_{m}^{-1}(\tilde{m}-b_{m})$ należy najpierw obliczyć odwrotność macierzy $K_{m}$. Dana jest ona wzorem $K_{m}^{-1}=\frac{1}{detK}(K_{m})_{D}^{T}$, gdzie $detK_{m}$ jest wyznacznikiem macierzy $K_{m}$
\begin{equation*}
detK_{m}=\varepsilon_{1}\varepsilon_{2}cos\rho_{1}\varepsilon_{3}cos\rho_{2}cos\rho_{3}
\end{equation*}
, a $(K_{m})_{D}^{T}$ jest macierzą dopełnień algebraicznych transponowaną.
\begin{equation*}
(K_{m})_{D}=
\begin{bmatrix}
a_{1} & a_{2} & a_{3} \\
a_{4} & a_{5} & a_{6} \\
a_{7} & a_{8} & a_{9} \\
\end{bmatrix}
\end{equation*}
\begin{equation*}
a_{1}=1\cdot \varepsilon_{1}cos\rho_{1}\varepsilon_{3}cos\rho_{2}cos\rho_{3}=\varepsilon_{1}cos\rho_{1}\varepsilon_{3}cos\rho_{2}cos\rho_{3} 
\end{equation*}
\begin{equation*}
a_{2}=-1\cdot \varepsilon_{2}sin\rho_{1}\varepsilon_{3}cos\rho_{2}cos\rho_{3}=-\varepsilon_{2}sin\rho_{1}\varepsilon_{3}cos\rho_{2}cos\rho_{3} 
\end{equation*}
\begin{equation*}
a_{3}=1\cdot( \varepsilon_{2}sin\rho_{1}\varepsilon_{3}sin\rho_{3}-\varepsilon_{2}cos\rho_{1}\varepsilon_{3}sin\rho_{2}cos\rho_{3})
\end{equation*}
\begin{equation*}
a_{4}=-1\cdot 0=0 
\end{equation*}
\begin{equation*}
a_{5}=1\cdot \varepsilon_{1}\varepsilon_{3}cos\rho_{2}cos\rho_{3}=\varepsilon_{1}\varepsilon_{3}cos\rho_{2}cos\rho_{3}
\end{equation*}
\begin{equation*}
a_{6}=-1\cdot \varepsilon_{1}\varepsilon_{3}sin\rho_{3}=-\varepsilon_{1}\varepsilon_{3}sin\rho_{3}
\end{equation*}
\begin{equation*}
a_{7}=1\cdot 0=0 
\end{equation*}
\begin{equation*}
a_{8}=-1\cdot 0=0 
\end{equation*}
\begin{equation*}
a_{9}=1\cdot \varepsilon_{1}\varepsilon_{2}cos\rho_{1}=\varepsilon_{1}\varepsilon_{2}cos\rho_{1}
\end{equation*}
\begin{equation*}
(K_{M})_{D}^{T}=
\begin{bmatrix}
a_{1} & 0 & 0 \\
a_{2} & a_{5} & 0 \\
a_{3} & a_{6} & a_{9} \\
\end{bmatrix}
\end{equation*}
Następnie zapisując równanie $m=K_{m}^{-1}(\tilde{m}-b_{m})$ w postaci macierzowej
\begin{equation*}
\begin{bmatrix}
m_{1}\\ m_{2}\\ m_{3}\\
\end{bmatrix}
=\frac{1}{detK_{m}}
\begin{bmatrix}
a_{1} & 0 & 0 \\
a_{2} & a_{5} & 0 \\
a_{3} & a_{6} & a_{9} \\
\end{bmatrix}
\begin{bmatrix}
\tilde{m}_{1}-\zeta_{1}\\
\tilde{m}_{2}-\zeta_{2}\\
\tilde{m}_{3}-\zeta_{3}\\
\end{bmatrix}
\end{equation*}
obliczamy składowe wektora $m$ ($detK_{m}=|K_{m}|$):
\begin{equation*}
m_{1}=\frac{a_{1}(\tilde{m}_{1}-\zeta_{1})}{|K_{m}|}
\end{equation*}
\begin{equation*}
m_{2}=\frac{a_{2}(\tilde{m}_{1}-\zeta_{1})}{|K_{m}|}+\frac{a_{5}(\tilde{m}_{2}-\zeta_{2})}{|K_{m}|}
\end{equation*}
\begin{equation*}
m_{3}=\frac{a_{3}(\tilde{m}_{1}-\zeta_{1})}{|K_{m}|}+\frac{a_{6}(\tilde{m}_{2}-\zeta_{2})}{|K_{m}|}+\frac{a_{9}(\tilde{m}_{3}-\zeta_{3})}{|K_{m}|}
\end{equation*}
, a następnie ich kwadraty:
\begin{equation*}
m_{1}^{2}=\frac{a_{1}^{2}}{|K_{m}|^{2}}(\tilde{m}_{1}^{2}-2\tilde{m}_{1}\zeta_{1}+\zeta_{1}^{2})
\end{equation*}
\begin{equation*}
m_{2}^{2}=\frac{a_{2}^{2}}{|K_{m}|^{2}}(\tilde{m}_{1}^{2}-2\tilde{m}_{1}\zeta_{1}+\zeta_{1}^{2})+\frac{2a_{2}a_{5}}{|K_{m}|^{2}}(\tilde{m}_{1}\tilde{m}_{2}-\tilde{m}_{1}\zeta_{2}-\tilde{m}_{2}\zeta_{1}+\zeta_{1}\zeta_{2})+\frac{a_{5}^{2}}{|K_{m}|^{2}}(\tilde{m}_{2}^{2}-2\tilde{m}_{2}\zeta_{2}+\zeta_{2}^{2})
\end{equation*}
\begin{multline*}
m_{3}^{2}=\frac{a_{3}^{2}}{|K_{m}|^{2}}(\tilde{m}_{1}^{2}-2\tilde{m}_{1}\zeta_{1}+\zeta_{1}^{2})+\frac{a_{6}^{2}}{|K_{m}|^{2}}(\tilde{m}_{2}^{2}-2\tilde{m}_{2}\zeta_{2}+\zeta_{2}^{2})+\frac{a_{9}^{2}}{|K_{m}|^{2}}(\tilde{m}_{3}^{2}-2\tilde{m}_{3}\zeta_{3}+\zeta_{3}^{2})+ \\
\frac{2a_{3}a_{6}}{|K_{m}|^{2}}(\tilde{m}_{1}\tilde{m}_{2}-\tilde{m}_{1}\zeta_{2}-\tilde{m}_{2}\zeta_{1}+\zeta_{1}\zeta_{2})+\frac{2a_{3}a_{9}}{|K_{m}|^{2}}(\tilde{m}_{1}\tilde{m}_{3}-\tilde{m}_{1}\zeta_{3}-\tilde{m}_{3}\zeta_{1}+\zeta_{1}\zeta_{3})+ \\
\frac{2a_{6}a_{9}}{|K_{m}|^{2}}(\tilde{m}_{2}\tilde{m}_{3}-\tilde{m}_{2}\zeta_{3}-\tilde{m}_{3}\zeta_{2}+\zeta_{2}\zeta_{3})
\end{multline*}
Dalej podstawiamy to do równania $m_{1}^{2}+m_{2}^{2}+m_{3}^{2}=|m|^{2}$ aby otrzymać współczynniki $C_{i}$ z równania \ref{eq2mag}.
\begin{equation*}
C_{1}=\frac{a_{1}^{2}+a_{2}^{2}+a_{3}^{2}}{|K_{m}|^{2}}
\end{equation*}
\begin{equation*}
C_{2}=\frac{2(a_{2}a_{5}+a_{3}a_{6})}{|K_{m}|^{2}}
\end{equation*}
\begin{equation*}
C_{3}=\frac{2a_{3}a_{9}}{|K_{m}|^{2}}
\end{equation*}
\begin{equation*}
C_{4}=\frac{a_{5}^{2}+a_{6}^{2}}{|K_{m}|^{2}}
\end{equation*}
\begin{equation*}
C_{5}=\frac{2a_{6}a_{9}}{|K_{m}|^{2}}
\end{equation*}
\begin{equation*}
C_{6}=\frac{a_{9}^{2}}{|K_{m}|^{2}}
\end{equation*}
\begin{equation*}
C_{7}=\frac{-2\zeta_{1}}{|K_{m}|^{2}}(a_{1}^{2}+a_{2}^{2}+a_{3}^{2})-\frac{-2\zeta_{2}}{|K_{m}|^{2}}(a_{2}a_{5}+a_{3}a_{6})-\frac{2a_{3}a_{9}\zeta_{3}}{|K_{m}|^{2}}
\end{equation*}
\begin{equation*}
C_{8}=\frac{-2\zeta_{1}}{|K_{m}|^{2}}(a_{2}a_{5}+a_{3}a_{6})-\frac{-2\zeta_{2}}{|K_{m}|^{2}}(a_{5}^{2}+a_{6}^{2})-\frac{2a_{6}a_{9}\zeta_{3}}{|K_{m}|^{2}}
\end{equation*}
\begin{equation*}
C_{9}=\frac{-2}{|K_{m}|^{2}}(a_{9}^{2}\zeta_{3}+a_{3}a_{9}\zeta_{1}+a_{6}a_{9}\zeta_{2})
\end{equation*}
\begin{multline*}
C_{10}=\frac{-\zeta_{1}^{2}}{|K_{m}|^{2}}(a_{1}^{2}+a_{2}^{2}+a_{3}^{2})-\frac{\zeta_{2}^{2}}{|K_{m}|^{2}}(a_{5}^{2}+a_{6}^{2})-\frac{2\zeta_{1}\zeta_{2}}{|K_{m}|^{2}}(a_{2}a_{5}+a_{3}a_{6}) \\
-\frac{a_{9}^{2}\zeta_{3}^{2}}{|K_{m}|^{2}}-\frac{2a_{3}a_{9}\zeta_{1}\zeta_{3}}{|K_{m}|^{2}}-\frac{2a_{6}a_{9}\zeta_{2}\zeta_{3}}{|K_{m}|^{2}}+|m|^{2}
\end{multline*}



